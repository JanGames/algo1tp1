package main;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;

public class Main {

	public static int[][] matriz0 = {
	                                 { 0,  7, -1,  5, -1, -1, -1},
	                                 { 7,  0,  2,  9,  1, -1, -1},
	                                 {-1,  2,  0, -1,  5, -1, -1},
	                                 { 5,  9, -1,  0, 15,  6, -1},
	                                 {-1,  1,  5, 15,  0,  1, 14},
	                                 {-1, -1, -1,  6,  1,  0, 11},
	                                 {-1, -1, -1, -1, 14, 11,  0}
									};
	//public static int[][] matriz;
	
	/*VALORES GENERALES
	public static int N = 5;
	public static int a = 1;
	public static int b = 2;
	public static int c = 3;
	public static int M = 70;
	public static int k = 40;
	public static int origen = 0;
	public static int destino = 1;*/
	
	public static void main(String[] args) {		
		//Genera la matriz de NxN
		//matriz = generar_matriz(N, a, b, c, M, k);
		
		int[][] cb = {
				{5, 1, 2, 3, 70, 40, 0, 1},
				{5, 1, 2, 3, 70, 40, 2, 4},
				{9, 3, 5, 7, 3739, 2468, 0, 4},
				{9, 3, 5, 7, 3739, 2468, 8, 5},
				{10, 3, 5, 7, 1037, 120, 6, 8},
				{10, 3, 5, 7, 1037, 120, 0, 2},
				{10, 3, 5, 7, 1037, 120, 6, 1},
		};
		
		//Creo camino de todos los casos base y printeo camino y costo
		int i = 0;
		while(i < cb.length) {
			int[][] matriz = generar_matriz(cb[i][0], cb[i][1], cb[i][2], cb[i][3], cb[i][4], cb[i][5]);
			List<Integer> camino = camino_minimo(matriz, cb[i][6], cb[i][7]);
			int costo = calcular_costo(matriz, lista_array(camino));
			System.out.println("Camino: " + camino + ", Costo: " + costo);
			i++;
		}
		
		/*
		System.out.println(camino_minimo(generar_matriz(5, 1, 2, 3, 70, 40), 0, 1));
		System.out.println(camino_minimo(generar_matriz(5, 1, 2, 3, 70, 40), 2, 4));
		System.out.println(camino_minimo(generar_matriz(9, 3, 5, 7, 3739, 2468), 0, 4));
		System.out.println(camino_minimo(generar_matriz(9, 3, 5, 7, 3739, 2468), 8, 5));
		System.out.println(camino_minimo(generar_matriz(10, 3, 5, 7, 1037, 120), 6, 8));
		System.out.println(camino_minimo(generar_matriz(10, 3, 5, 7, 1037, 120), 0, 2));
		System.out.println(camino_minimo(generar_matriz(10, 3, 5, 7, 1037, 120), 6, 0));
		*/
	}
	
	public static int[][] generar_matriz(int N, int a, int b, int c, int M, int k)
	{
		int i = 0, j = 0;
		int[][] nMatriz = new int[N][N];
		
		while(i < nMatriz.length) {
			while(j < nMatriz[i].length) {
				int valor = (int) ((Math.pow((i - j), 2) * (a * Math.pow(2, i+j) + b * i * j + c)) % M);
				//Modulo de java va entre -resto y resto, como no quiero costos negativos le sumo M en caso de que sea negativo
				if(valor < 0) { valor += M; }
				
				if(valor <= k) { nMatriz[i][j] = valor; }
				else { nMatriz[i][j] = -1; }
				j++;
			}
			
			i++;
			j = 0;
		}
		
		return nMatriz;
	}
	
	public static void actualizar_vecinos (int[][] matriz, int vertice, int[] padre, int[] costo)
	{
		int i = 0;
		int nCosto = 0;
		
		while(i < padre.length) {
			if(matriz[vertice][i] != -1) {
				nCosto = costo[vertice] + matriz[vertice][i];
				
				if(costo[i] == -1 || nCosto < costo[i]) {
					padre[i] = vertice;
					costo[i] = nCosto;
				}
			}
			
			i++;
		}
	}
	
	public static boolean existe_alcanzable(int[] padre, int[] uso)
	{
		int i = 0;
		boolean exists = false;
		
		while(i < padre.length) {
			if(padre[i] != -1 && uso[i] == 0) { exists = true; }
		
			i++;
		}
		
		return exists;
		
	}
	
	public static int buscar_minimo(int[] costo, int[] uso, int[] padre)
	{
		int i = 0;
		int minimo = -1;
		
		while(i < uso.length) {
			if(uso[i] == 0 && padre[i] != -1 && (minimo == -1 || costo[i] < costo[minimo])) { minimo = i; }
			
			i++;
		}
		
		return minimo;
	}
	
	public static List<Integer> generar_camino (int[] padre, int origen, int destino)
	{
		List<Integer> camino = new ArrayList<Integer>();

		int i = destino;
		while(i != -1 && i != origen) {
			camino.add(0, i);
			i = padre[i];
		}
		if(i != -1) { camino.add(0, i);	}
		else { camino.clear(); }
		
		return camino;
	}
	
	public static List<Integer> camino_minimo (int[][] adj , int origen, int destino)
	{
		int[] uso = new int[adj.length];
		int[] padre = new int[adj.length];
		int[] costo = new int[adj.length];
		Arrays.fill(uso, 0);
		Arrays.fill(padre, -1);
		Arrays.fill(costo, -1);
		
		int i = origen;
		indicar_origen(uso, padre, costo, i);
		while(existe_alcanzable(padre, uso) && i != destino) {
			uso[i] = 1;
			actualizar_vecinos(adj, i, padre, costo);
			i = buscar_minimo(costo, uso, padre);
		}
		
		return generar_camino(padre, origen, destino);
	}
	
	public static int calcular_costo(int[][] matriz, int[] camino) {
		int i = 0;
		int costo = 0;
		
		while(i < camino.length - 1) {
			costo += matriz[camino[i]][camino[i + 1]];
			i++;
		}
		
		return costo;
	}

	public static void indicar_origen(int[] uso, int[] padre, int[] costo, int vertice)
	{
		uso[vertice] = 0;
		padre[vertice] = vertice;
		costo[vertice] = 0;
	}
	
	public static int[] lista_array(List<Integer> lista) {
		int[] array = new int[lista.size()];
		int i = 0;
		while(i < array.length) {
			array[i] = lista.get(i);
			i++;
		}
		return array;
	}
	
	/*
	 * -----------------------------------------------------------------------
	 * A PARTIR DE ACA SON TODAS FUNCIONES AUXILIARES NO NECESARIAS PARA EL TP
	 * -----------------------------------------------------------------------
	 */
	public static void print_matriz(int[][] matriz)
	{
		//Crea indices i y j, crea string
		int i = 0, j = 0;
		String mRow = "";
		//Crea primera fila de vx para referencia
		mRow = "\t";
		while(i < matriz.length) {
			mRow += "v" + i + "\t";
			i++;
		}
		System.out.println(mRow);
		mRow = "";
		i = 0;
		//Recorre matriz por i vertical y j horizontal y crea filas de ints, agrega vx al principio
		while(i < matriz.length) {
			while(j < matriz[i].length) {
				mRow += matriz[i][j] + ",\t";
				j++;
			}
			//Printea linea en consola
			System.out.println("v" + i + "\t" + mRow);
			//Reinicia loop en j
			mRow = "";
			i++;
			j = 0;
		}
	}
	
	public static void print_lista(int[] uso, int[] padre, int[] costo)
	{
		//Imprimo los valores de las listas
		int i = 0;
		String lRow = "Uso\t";
		while(i < uso.length) {
			lRow += uso[i] + "\t";
			i++;
		}
		System.out.println(lRow);
		lRow = "Padre\t";
		i = 0;
		while(i < padre.length) {
			lRow += padre[i] + "\t";			
			i++;
		}
		System.out.println(lRow);
		lRow = "Costo\t";
		i = 0;
		while(i < costo.length) {
			lRow += costo[i] + "\t";			
			i++;
		}
		System.out.println(lRow);
	}

}